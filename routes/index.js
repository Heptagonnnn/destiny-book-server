const Router = require("koa-router");
const util = require("util");
const fs = require("fs");
const readFile = util.promisify(fs.readFile);

const router = new Router();



router.get("/data", async (ctx, next) => {
  const res = await readFile("./public/data.json").then(res => {
    return res.toString();
  })

  ctx.set("Content-Type", "application/json");
  ctx.body = res;
  ctx.status = 200;
});


module.exports = router;